from flask import Flask
from utilities import EnvirementVar, Packages

class Elizabeth:
    def __init__(self) -> None:
        self.app = Flask(__name__)
        self.endpoints = []

    def load_routes(self, packages : dict):

        # Create route for every package
        # ROUTE: /package_{name}
        for key, value in packages.items():
            self.app.add_url_rule(f"/{key}", key.split("_")[1], value.initialize)
            self.endpoints.append(key.split("_")[1])

        # ROUTE: /update_packages
        self.app.add_url_rule("/update_packages", "update_packages", Packages.update_packages)

        # TODO
        # ROUTE: /load_packages

        # Separate routes to different file (if its possible)


    def start(self):
        packages = Packages().load_packages()
        self.load_routes(packages)
        self.app.run(debug=True)

if __name__ == "__main__":
    Elizabeth().start()
