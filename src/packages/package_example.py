class example:   
    def __init__(self) -> None:
        self.keywords = [
            "keyword1",
            "keyword2"
        ]

    def doSmth(self):
        return self.keywords

def get_keywords():
    return example().keywords

def initialize():
    return example().doSmth()