import openai

"""
Class to work with chat-gpt API
"""

class ChatBOT:
    def __new__(self, message, key) -> str:
        self.api_key = key
        self.message = message

        return self.send_message(self)

    """
    RETURN:
        - string  # response of the API
    """
    def send_message(self) -> str:
        openai.api_key = self.api_key

        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo", 
            messages=[
                {
                    "role": "user", 
                    "content": self.message
                }
            ]
        )

        return response.choices[0].message.content
    

