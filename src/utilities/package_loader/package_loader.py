import importlib
import pathlib
from utilities import Database, EnvirementVar

class Packages:
    def __init__(self) -> None:
        self.database = Database(
            EnvirementVar.MONGODB_URL, 
            EnvirementVar.DATABASE_NAME, 
            EnvirementVar.COLLECTION_NAME
        )
        self.packages = {}

    def load_packages(self):
        # TODO: move to compare_packages

        packages = {}
        package_names = set(_.stem for _ in pathlib.Path.glob(pathlib.Path("./packages"), "package_*.py"))
        
        # TODO

        # 1. self.compare_packages
        # 2. Save to self.packages

        for package in package_names:
            print(package)
            pac = importlib.import_module(f"packages.{package}")
            packages[package] = pac
        
        return packages
    
    def update_packages(self):
        # TODO

        # 1. self.compare_packages
        # 2. Update packages states
        # 3. Save to self.packages

        pass
    
    def compare_packages(self):
        # TODO

        # 1. self.get_packages():
        # 2. Compare with packages from ./package
        # 3. Compare packages states from self and from frontend

        # RETURN: updated packages
        pass

    def get_packages(self):
        return self.packages
    