import pymongo

class Database:
    def __init__(self, url, database_name, collection_name) -> None:
        self.client = pymongo.MongoClient(url)
        self.database = self.client[database_name]
        self.collection_name = self.database[collection_name]

    def insert_package(self, data : dict):
        self.collection_name.insert_one(data)
    
    def check_user_package(self, machine_id):
        self.collection_name.find({ 'id' : machine_id})
    