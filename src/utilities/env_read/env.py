from dotenv import dotenv_values


class EnvirementVar:  
    dotenv_var = {
        **dotenv_values(".env") 
    }

    CHAT_GPT_API_KEY = dotenv_var["CHAT_GPT_KEY"]
    MONGODB_URL = dotenv_var["MONGO_DB_URL"]
    DATABASE_NAME = dotenv_var["DATABASE_NAME"]
    COLLECTION_NAME = dotenv_var["COLLECTION_NAME"]
    
    